$(document).ready(function () {
    var firstSwiper = new Swiper ('.swiper-container', {
      	loop: true,
      	grabCursor: true,
    	navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      	},
    });
 });